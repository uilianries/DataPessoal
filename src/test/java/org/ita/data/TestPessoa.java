/*!
 * @file TestPessoa.java
 * @brief Teste unitários para Pessoa
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
package org.ita.data;

import org.junit.Test;

import java.time.Month;
import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

/*!
 * @brief Executa teste da classe Pessoa
 */
public class TestPessoa {
    /*! Pessoa compartilhada */
    private Pessoa pessoa = null;

    @Test
    public void testPessoaAries() {
        pessoa = new Pessoa(LocalDate.of(1989, Month.APRIL, 1));
        assertEquals(27, pessoa.getIdade());
        assertEquals(Signo.Entidade.ARIES, pessoa.getSigno());
    }

    @Test
    public void testPessoaGemeos() {
        pessoa = new Pessoa(LocalDate.of(1989, Month.MAY, 29));
        assertEquals(27, pessoa.getIdade());
        assertEquals(Signo.Entidade.GEMEOS, pessoa.getSigno());
    }

    @Test
    public void testPessoaCancer() {
        pessoa = new Pessoa(LocalDate.of(2007, Month.JUNE, 24));
        assertEquals(9, pessoa.getIdade());
        assertEquals(Signo.Entidade.CANCER, pessoa.getSigno());
    }
}
