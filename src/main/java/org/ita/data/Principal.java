/*!
 * @file Principal.java
 * @brief Executa programa
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
package org.ita.data;

import java.time.LocalDate;
import java.time.Month;

public class Principal {

    public static void main(String[] args) {
        final LocalDate nascimento = LocalDate.of(1989, Month.JANUARY, 7);
        Pessoa pessoa = new Pessoa(nascimento);
        System.out.println("Quem nasceu em " + nascimento + " possui " + pessoa);
    }
}
