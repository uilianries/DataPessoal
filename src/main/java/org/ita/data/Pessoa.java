/*!
 * @file Pessoa.java
 * @brief Representação para uma pessoa
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
package org.ita.data;

import java.time.LocalDate;
import java.time.MonthDay;
import java.time.temporal.ChronoUnit;

/*!
 * @brief Classe que representa um Pessoa
 */
public class Pessoa {
    /*! Idade da pessoa */
    private int idade;
    /*! Signo da pessoa */
    private Signo signo;

    /*!
     * @brief Constroi uma pessoa com data de nascimento
     * @param dataNascimento  Data de nascimento da pessoa
     */
    public Pessoa(LocalDate dataNascimento) {
        this.idade = (int) dataNascimento.until(LocalDate.now(), ChronoUnit.YEARS);
        this.signo = new Signo(MonthDay.of(dataNascimento.getMonth(), dataNascimento.getDayOfMonth()));
    }

    /*!
     * @brief Calcula idade da pessoa
     * @return Idade atual da pessoa
     */
    public int getIdade() {
        return this.idade;
    }

    /*!
     * @brief Recupera signo da pessoa
     * @return Entidade do signo da pessoa
     */
    public Signo.Entidade getSigno() {
        return this.signo.getEntidade();
    }

    @Override
    public String toString() {
        return "Idade: " + this.idade + "\tSigno: " + this.signo;
    }

}
